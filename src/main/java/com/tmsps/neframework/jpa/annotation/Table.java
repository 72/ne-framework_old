package com.tmsps.neframework.jpa.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据库表明注解
 * 
 * @author zhangwei 396033084@qq.com 2015年6月15日上午10:19:09
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Table {
	public String TableName() default "";
}
