package com.tmsps.neframework.jpa.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据库连接池注解
 * 
 * @author zhangwei 396033084@qq.com 2015年6月15日上午10:18:10
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface DataSource {
	boolean showSql() default false;
}
