package com.tmsps.neframework.jpa.base;

import java.util.logging.Logger;

import com.tmsps.neframework.jpa.sql.JdbcComponent;
import com.tmsps.neframework.jpa.sql.JdbcNewComponent;
import com.tmsps.neframework.jpa.sql.OrmComponent;

public class BaseService {
	// 对象操作组件
	public final OrmComponent oc = new OrmComponent();
	// sql操作组件
	public final JdbcComponent jc = new JdbcComponent();

	public final JdbcNewComponent jcn = new JdbcNewComponent();
	
	public Logger logger = Logger.getLogger("ne framework ORM");

	// // mongo 操作
	// public static DB db = null;
	//
	// static {
	// try {
	// MongoClient mc = new MongoClient(Config.MONGO_HOST, Config.MONGO_PORT);
	// db = mc.getDB(Config.MONGO_DB);
	// } catch (UnknownHostException e) {
	// e.printStackTrace();
	// }
	// }

}
