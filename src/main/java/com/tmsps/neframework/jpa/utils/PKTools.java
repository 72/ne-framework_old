/**
 * 2015年6月9日下午5:09:53   2015
 * @author zhangwei  396033084@qq.com
 * @Description: 
 */
package com.tmsps.neframework.jpa.utils;

import com.tmsps.neframework.core.utils.GenerateTools;

/**
 * PKTools 主键工具
 * 
 * @author zhangwei 396033084@qq.com 2015年6月9日下午5:09:53
 */
public class PKTools {
	/**
	 * 获取Base58后的主键
	 * 
	 * @author zhangwei
	 */
	public static String getPK() {
		return GenerateTools.getBase58ID();
	}
}
