package com.tmsps.neframework.jpa.utils;

import java.lang.reflect.Field;

import com.tmsps.neframework.jpa.annotation.NotMap;

public class SqlHelperTools {

	public static String getModelAllFileds(Class<?> clazz, String table) {
		String sql = "";
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(NotMap.class)) {
				continue;
			}
			sql += table + "." + field.getName() + ",";
		}
		return sql;
	}

}
