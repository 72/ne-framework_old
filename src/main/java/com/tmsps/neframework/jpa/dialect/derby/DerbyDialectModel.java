package com.tmsps.neframework.jpa.dialect.derby;

import com.tmsps.neframework.jpa.dialect.basic.DialectModel;

public class DerbyDialectModel implements DialectModel {

	public String getPageSql() {
		return " OFFSET :start ROWS FETCH NEXT :pageSize ROWS ONLY ";
	}

	public CharSequence getSplitChar() {
		return "";
	}

}
