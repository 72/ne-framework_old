package com.tmsps.neframework.jpa.dialect;

import com.tmsps.neframework.jpa.base.DatabaseInfo;
import com.tmsps.neframework.jpa.base.DatabaseInfo.Dialect;
import com.tmsps.neframework.jpa.dialect.basic.DialectModel;
import com.tmsps.neframework.jpa.dialect.derby.DerbyDialectModel;
import com.tmsps.neframework.jpa.dialect.mysql.MysqlDialectModel;

/**
 * 方言的描述信息
 * 
 * @author 冯晓东
 *
 */
public class DialectInfo {

	private static DialectModel dialectModel = null;

	static {
		Dialect dialect = DatabaseInfo.getDialect();

		if (dialect == DatabaseInfo.Dialect.derby) {
			dialectModel = new DerbyDialectModel();
		} else if (dialect == DatabaseInfo.Dialect.mysql) {
			dialectModel = new MysqlDialectModel();
		} else {

		}

	}

	public static DialectModel getDialectModel() {
		return dialectModel;
	}

	// 获取分页数据
	public static String getPageSql() {
		return dialectModel.getPageSql();
	}

	// 获取关键字分割字符
	public static CharSequence getSplitChar() {
		return dialectModel.getSplitChar();

	}

}
