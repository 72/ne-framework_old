package com.tmsps.neframework.jpa.dialect.mysql;

import com.tmsps.neframework.jpa.dialect.basic.DialectModel;

public class MysqlDialectModel implements DialectModel {

	public String getPageSql() {
		return " limit :start,:pageSize ";
	}

	public CharSequence getSplitChar() {
		return "`";
	}

}
