package com.tmsps.neframework.jpa.dialect.basic;

public interface DialectModel {

	/**
	 * 分页语法
	 * 
	 * :start 开始
	 * 
	 * :pageSize 每页条数
	 * 
	 */
	public String getPageSql();

	/**
	 * 获取关键字分割字符
	 */
	public CharSequence getSplitChar();

}
