package com.tmsps.neframework.jpa.sql;

import java.util.List;
import java.util.Map;

import com.tmsps.neframework.core.common.page.Page;
import com.tmsps.neframework.jpa.sql.param.NeParamList;
import com.tmsps.neframework.jpa.sql.param.NeParamTools;

public class JdbcNewComponent {

	protected JdbcComponent jc = new JdbcComponent();

	public List<Map<String, Object>> queryForList(String sql, NeParamList params, Map<String, String> sort_params, Page page) {

		String endSql = NeParamTools.handleSql(sql, params);

		return jc.queryForList(endSql, params.getParamValues(), sort_params, page);
	}

}
