package com.tmsps.neframework.jpa.core;

import java.util.ArrayList;
import java.util.List;

import com.tmsps.neframework.core.utils.ClassScanTools;

public class CoreQueue {

	// 程序的所有类
	public static List<Class<?>> allClasses = new ArrayList<Class<?>>();

	static {
		init();
	}

	private static void init() {
		// 初始化所有程序
		allClasses = ClassScanTools.getClasses("");
	}

}
