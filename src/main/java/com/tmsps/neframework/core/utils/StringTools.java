package com.tmsps.neframework.core.utils;

public class StringTools {
	public static String splitStrToSqlIn(String codes) {
		//转换普通字符串为 sql in 字符数组
		String end = "''";
		if (ChkTools.isNull(codes)) {
			return end;
		}
		for (String c : codes.split(",")) {
			end += ",'" + c + "'";
		}
		return end;
	}

}
