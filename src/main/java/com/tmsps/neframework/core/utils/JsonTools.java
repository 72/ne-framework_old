package com.tmsps.neframework.core.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class JsonTools {

	/**
	 * json 字符串 转 Map
	 */
	public static Map<String, Object> jsonStrToMap(String json) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (ChkTools.isNull(json)) {
			return map;
		}
		JSONObject jsonObject = JSON.parseObject(json);
		Set<String> keys = jsonObject.keySet();
		for (String key : keys) {
			map.put(key, jsonObject.get(key));
		}

		return map;
	}

	/**
	 * json 字符串 转 JSONObject
	 */
	public static Object jsonStrToJsonObject(String json, Class<?> clazz) {
		if (clazz == String.class) {
			if (json.length() > 2) {
				// 去掉首尾的引号
				json = json.substring(1, json.length() - 1);
			}
			return json;
		} else {
			JSONObject parse = JSON.parseObject(json);
			return JSON.toJavaObject(parse, clazz);
		}
	}

	/**
	 * 对象 转 json
	 */
	public static String toJson(Object obj) {
		if (obj == null) {
			return "";
		}
		return JSON.toJSONString(obj);
	}

	// 设置 json 前缀
	public static String toJson(String pre, Object obj) {
		Map<String, Object> map = objToMap(obj);
		Map<String, Object> mapEnd = new HashMap<String, Object>();
		if (ChkTools.isNotNull(pre)) {
			pre = pre + ".";
		}
		for (Entry<String, Object> ent : map.entrySet()) {
			mapEnd.put(pre + ent.getKey(), ent.getValue());
		}

		return JSON.toJSONString(mapEnd);
	}

	// 设置 json 前缀
	public static Map<String, Object> objToMap(String pre, Object obj) {
		Map<String, Object> map = objToMap(obj);
		Map<String, Object> mapEnd = new HashMap<String, Object>();
		if (ChkTools.isNotNull(pre)) {
			pre = pre + ".";
		}
		for (Entry<String, Object> ent : map.entrySet()) {
			mapEnd.put(pre + ent.getKey(), ent.getValue());
		}

		return mapEnd;
	}

	public static Map<String, Object> objToMap(Object obj) {
		String json = toJson(obj);
		Map<String, Object> map = jsonStrToMap(json);
		return map;
	}

	public static String toJson(Map<String, Object>... maps) {
		//将多个map转换为json
		Map<String, Object> mapEnd = new HashMap<String, Object>();
		for (Map<String, Object> map : maps) {
			for (Entry<String, Object> ent : map.entrySet()) {
				mapEnd.put(ent.getKey(), ent.getValue());
			}
		}
		return JsonTools.toJson(mapEnd);
	}
}
