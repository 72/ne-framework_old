package com.tmsps.neframework.core.log;

import java.util.logging.Logger;

public class NeLog {

	private static Logger log = Logger.getLogger("Ne Log");

	public static void warning(Object obj) {
		if (obj == null) {
			log.warning("");
		}
		log.warning(obj.toString());
	}

	public static void info(Object obj) {
		if (obj == null) {
			log.info("");
		}
		log.info(obj.toString());
	}

	public static void fine(Object obj) {
		if (obj == null) {
			log.fine("");
		}
		log.fine(obj.toString());
	}

}
