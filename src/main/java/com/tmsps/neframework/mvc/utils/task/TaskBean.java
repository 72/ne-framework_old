package com.tmsps.neframework.mvc.utils.task;

import java.lang.reflect.Method;

/**
 * 任务Bean
 *
 */
public class TaskBean {

	private String code;

	private Object service;
	private Class<?> clazz;
	private Method method;

	private String tip;
	private String cntStr;

	// ============ get / set () =======================
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public Object getService() {
		return service;
	}

	public void setService(Object service) {
		this.service = service;
	}

	public String getCntStr() {
		return cntStr;
	}

	public void setCntStr(String cntStr) {
		this.cntStr = cntStr;
	}

}
