package com.tmsps.neframework.mvc.utils.task;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tmsps.neframework.core.utils.ChkTools;
import com.tmsps.neframework.mvc.annotation.task.Task;
import com.tmsps.neframework.mvc.core.CoreQueue;

public class TaskTools {

	// 初始化 Task
	public static void initTasks(List<Class<?>> allClasses, Map<String, TaskBean> tasks) {
		// TODO Auto-generated method stub

		// 遍历所有 类
		for (Class<?> serviceClass : allClasses) {
			Method[] methods = serviceClass.getMethods();
			// 遍历所有 函数
			for (Method method : methods) {
				if (!method.isAnnotationPresent(Task.class)) {
					continue;
				}
				Task taskAnno = method.getAnnotation(Task.class);

				TaskBean tb = new TaskBean();
				tb.setCode(taskAnno.code());
				tb.setClazz(serviceClass);
				try {
					tb.setService(serviceClass.newInstance());
				} catch (Exception e) {
					e.printStackTrace();
				}
				tb.setMethod(method);
				tb.setTip(taskAnno.tip());
				tb.setCntStr(taskAnno.cnt());

				tasks.put(taskAnno.code(), tb);
			}

		}
	}

	// 获取某 code 的任务数
	public static Map<String, Object> getTaskBean(String code) {
		Long cnt = 0L;

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", code);
		map.put("cnt", cnt);
		map.put("tip", null);

		TaskBean tb = CoreQueue.tasks.get(code);
		if (tb == null) {
			return map;
		}
		Method method = tb.getMethod();
		try {
			cnt = (Long) method.invoke(tb.getService());
		} catch (Exception e) {
			System.err.println("Task任务出错,请规范写法----------->>" + method);
			e.printStackTrace();
		}

		map.put("cnt", cnt);
		map.put("tip", tb.getTip().replace(tb.getCntStr(), cnt + ""));

		return map;
	}

	// 循环获取所有任务
	public static List<Map<String, Object>> loopTask(String codes) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		if (ChkTools.isNull(codes)) {
			return list;
		}

		for (String code : codes.split(",")) {
			if (!CoreQueue.tasks.containsKey(code)) {
				continue;
			}
			Map<String, Object> map = TaskTools.getTaskBean(code);
			list.add(map);
		}
		return list;
	}
}
