package com.tmsps.neframework.mvc.core;

import java.util.Map;
import java.util.Set;

import com.tmsps.neframework.core.utils.ChkTools;
import com.tmsps.neframework.mvc.core.ControlModel;
import com.tmsps.neframework.mvc.core.CoreQueue;

public class ActionInvoke {
	/**
	 * 根据 请求 url，获取控制模型
	 */
	public static ControlModel getControllModel(String webUrl, Map<String, ControlModel> controlMap) {
		if (ChkTools.isNull(webUrl) || webUrl.endsWith(".jsp")) {
			return null;
		}
		webUrl = cutSuffix(webUrl);
		ControlModel cm = null;

		// 搜索普通url
		cm = controlMap.get(webUrl);

		// 搜索rest
		if (cm == null) {
			Set<String> restUrls = CoreQueue.restControlMap.keySet();
			for (String restUrl : restUrls) {
				if (webUrl.matches(restUrl)) {
					cm = CoreQueue.restControlMap.get(restUrl);
					break;
				}
			}

		}
		return cm;
	}

	public static String getSuffix(String filename) {
		if (ChkTools.isNull(filename)) {
			return "";
		}
		if (!filename.contains(".")) {
			return "";
		}
		return filename.substring(filename.lastIndexOf(".") + 1);
	}

	public static String cutSuffix(String string) {
		if (ChkTools.isNull(string) || !string.contains(".")) {
			return string;
		}
		string = string.substring(0, string.indexOf("."));
		return string;

	}
}
