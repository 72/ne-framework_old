package com.tmsps.neframework.mvc.core;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmsps.neframework.core.utils.ChkTools;
import com.tmsps.neframework.core.utils.JsonTools;
import com.tmsps.neframework.mvc.annotation.ResponseBody;
import com.tmsps.neframework.mvc.annotation.ResponseBodyP;
import com.tmsps.neframework.mvc.utils.FormTools;
import com.tmsps.neframework.mvc.utils.WebTools;
import com.tmsps.neframework.mvc.web.interceptor.InteceptorInvoke;

public class DispatcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Logger log = Logger.getLogger("ne framework DispatcherServlet");

	private String viewPath = "";

	public void init() throws ServletException {
		viewPath = this.getInitParameter("view-path");
	}

	protected void doMine(HttpServletRequest req, HttpServletResponse resp,
			Map<String, ControlModel> controlMap) throws ServletException,
			IOException {
		// 获取web访问的地址url
		String webUrl = WebTools.getUri(req);
		log.info(webUrl);

		// 封装Controller(action)映射到map
		ControlModel cm = ActionInvoke.getControllModel(webUrl, controlMap);
		if (cm == null) {
			log.info("end -404-->" + webUrl);
			resp.sendError(404);
			return;
		}

		Class<?> actionClass = cm.getAction();
		Method method = cm.getMethod();
		Pattern restPattern = cm.getRestPattern();

		try {
			// 初始化action实例
			NeBaseController action = (NeBaseController) actionClass
					.newInstance();
			action.setReq(req);
			action.setResp(resp);

			// 执行前置拦截器
			boolean before = InteceptorInvoke.invokeBefore(action, method, req,
					resp);
			if (before == false) {
				return;
			}

			// 封装 form bean
			List<Object> params = null;

			if (restPattern != null) {
				params = FormTools.getFormBeanRest(method, webUrl, restPattern,
						req, resp);
			} else {
				params = FormTools.getFormBean(method, req, resp);
			}

			Object invoke = method.invoke(action, params.toArray());
			if (invoke != null && method.getReturnType() == String.class) {
				action.return_url = invoke.toString();
			}

			// 执行后置拦截器
			boolean after = InteceptorInvoke.invokeAfter(action, method, req,
					resp);
			if (after == false) {
				return;
			}

			/**
			 * 判断返回值,确定重定向还是转发
			 */
			// ajax 方式
			if (method.isAnnotationPresent(ResponseBody.class)) {
				resp.setCharacterEncoding("utf-8");
				if (method.getReturnType() == String.class) {
					resp.getWriter().print(action.return_url);
				} else {
					resp.getWriter().print(JsonTools.toJson(action.result));
				}
				resp.getWriter().flush();

				return;
			} else if (method.isAnnotationPresent(ResponseBodyP.class)) {
				resp.setCharacterEncoding("utf-8");

				String callback = req.getParameter("callback") + "(@param@)";

				if (method.getReturnType() == String.class) {
					resp.getWriter().print(
							callback.replace("@param@", action.return_url));
				} else {
					resp.getWriter().print(
							callback.replace("@param@",
									JsonTools.toJson(action.result)));
				}
				resp.getWriter().flush();

				return;
			} else if (ChkTools.isNotNull(action.return_url)) {

				// URL 方式
				String return_url = action.return_url;

				if (return_url.startsWith("redirect:")) {
					return_url = return_url
							.substring(return_url.indexOf(":") + 1);

					String end_url = null;
					if (return_url.startsWith("http://")) {
						end_url = return_url;
					} else if (return_url.startsWith("/")) {
						end_url = req.getContextPath() + return_url;
					} else {
						int lastIndexOf = webUrl.lastIndexOf("/");
						end_url = req.getContextPath()
								+ webUrl.substring(0, lastIndexOf) + "/"
								+ return_url;
					}
					resp.sendRedirect(end_url);// 重定向

				} else {

					if (WebTools.isForward(req)) {
						return_url = viewPath + return_url;
						req.getRequestDispatcher(return_url).forward(req, resp);// 转发
					} else {
						return_url = viewPath + return_url;
						// jsp:include 的情况
						req.getRequestDispatcher(return_url).include(req, resp);
					}
				}
			} else {
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doMine(req, resp, CoreQueue.controlMapGet);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doMine(req, resp, CoreQueue.controlMapPost);
	}// #doPost
}
