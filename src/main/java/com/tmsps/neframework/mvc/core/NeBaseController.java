package com.tmsps.neframework.mvc.core;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class NeBaseController {

	public HttpServletRequest req;
	public HttpServletResponse resp;

	public Logger logger = Logger.getLogger("ne framework");

	// 返回结果
	public Map<String, Object> result = new HashMap<String, Object>();
	public String return_url = null;

	// ========= get / set ()=========================
	public void setReq(HttpServletRequest req) {
		this.req = req;
	}

	public void setResp(HttpServletResponse resp) {
		this.resp = resp;
	}

}
