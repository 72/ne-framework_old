package com.tmsps.neframework.mvc.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * action的访问地址,类上面的和方法名上面的拼装和web访问的url
 * 
 * @author zhangwei 396033084@qq.com 2015年6月15日上午10:23:21
 */
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RequestMapping {

	public String value();

	public String method() default "";
}
