package com.tmsps.neframework.mvc.annotation.task;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Task 任务注解
 * 
 * @author 冯晓东
 * 
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Task {
	String code();

	String tip() default "";

	String cnt() default "@cnt";
}
