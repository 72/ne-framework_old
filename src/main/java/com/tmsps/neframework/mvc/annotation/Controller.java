package com.tmsps.neframework.mvc.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 控制器: 代表被注解的类 为 action
 * 
 * @author zhangwei 396033084@qq.com 2015年6月15日上午10:21:13
 */

@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Controller {

}
