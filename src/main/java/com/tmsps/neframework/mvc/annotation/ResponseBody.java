package com.tmsps.neframework.mvc.annotation;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 返回json形式
 * 
 * @author zhangwei 396033084@qq.com 2015年6月15日上午10:23:47
 */

@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ResponseBody {

}
