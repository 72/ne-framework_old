package com.tmsps.neframework.mvc.web.interceptor;

import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmsps.neframework.mvc.core.NeBaseController;
import com.tmsps.neframework.mvc.core.CoreQueue;
import com.tmsps.neframework.mvc.utils.WebTools;
import com.tmsps.neframework.mvc.web.interceptor.Interceptor;
import com.tmsps.neframework.mvc.web.interceptor.InterceptorModel;

public class InteceptorInvoke {

	public static boolean invokeBefore(NeBaseController action, Method method, HttpServletRequest req, HttpServletResponse resp) {
		String webUrl = WebTools.getUri(req);

		for (InterceptorModel interceptor : CoreQueue.interceptors) {
			if (webUrl.startsWith(interceptor.getUrl())) {
				// 属于拦截器范畴才拦截
				List<Interceptor> list = interceptor.getList();
				for (Interceptor in : list) {
					boolean before = in.before(action, method, req, resp);
					if (before == false) {
						return false;
					}
				}

			}

		}// #for

		return true;
	}

	public static boolean invokeAfter(NeBaseController action, Method method, HttpServletRequest req, HttpServletResponse resp) {
		String webUrl = WebTools.getUri(req);

		for (InterceptorModel interceptor : CoreQueue.interceptors) {
			if (webUrl.startsWith(interceptor.getUrl())) {
				// 属于拦截器范畴才拦截
				List<Interceptor> list = interceptor.getList();
				for (Interceptor in : list) {
					boolean after = in.after(action, method, req, resp);
					if (after == false) {
						return false;
					}
				}

			}

		}// #for

		return true;
	}

}
