package com.tmsps.neframework.mvc.web.interceptor;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmsps.neframework.mvc.core.NeBaseController;

public interface Interceptor {

	public boolean before(NeBaseController action, Method method, HttpServletRequest request, HttpServletResponse response);

	public boolean after(NeBaseController action, Method method, HttpServletRequest request, HttpServletResponse response);

}
