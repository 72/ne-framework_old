package com.tmsps.neframework.test.jpa;

import java.util.ArrayList;
import java.util.List;

import com.tmsps.neframework.jpa.model.DbModel;
import com.tmsps.neframework.jpa.sql.OrmComponent;
import com.tmsps.neframework.test.model.t_fk_user;

public class TranscationTest {

	public static void main(String[] args) {
		OrmComponent oc = new OrmComponent();

		List<DbModel> objs = new ArrayList<DbModel>();
		t_fk_user u = new t_fk_user();
		u.setName("test1");
		oc.saveOrUpdateObjs(objs);

		t_fk_user u1 = new t_fk_user();
		u1.setName("test--u1");
		oc.saveObj(u1);
	}
}
