package com.tmsps.neframework.test.jpa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.tmsps.neframework.jpa.base.DatabaseInfo;
import com.tmsps.neframework.jpa.utils.DatabaseTools;

public class TranscationConnectionTest {

	public void execute() {

		String sql1 = "update t_fk_log set dept_code='1' where kid='G79yVrnyQp58T8jsV4CBZu'";

		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DatabaseInfo.getConn();
			conn.setAutoCommit(false);

			pstmt = conn.prepareStatement(sql1);
			boolean execute = pstmt.execute();
			System.err.println(execute);
			conn.commit();

			String sql2 = "update t_fk_log set dept_code='2' where kid='G79yVrnyQp58T8jsV4CBZu'";
			PreparedStatement pstmt2 = conn.prepareStatement(sql2);
			pstmt2.execute();

		} catch (SQLException e) {
			e.printStackTrace();
			if (conn != null) {
				try {
					conn.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		} finally {
			DatabaseTools.close(null, pstmt, conn);
		}
	}

	public static void main(String[] args) {
		TranscationConnectionTest s = new TranscationConnectionTest();
		s.execute();
	}
}
