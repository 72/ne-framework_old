package com.tmsps.neframework.test.model;

import java.sql.Timestamp;

import com.tmsps.neframework.jpa.annotation.Id;
import com.tmsps.neframework.jpa.annotation.NotMap;
import com.tmsps.neframework.jpa.annotation.Table;
import com.tmsps.neframework.jpa.model.DbModel;

/**
 * 用户表
 * 
 * @author 冯晓东 398479251
 *
 */
@Table
public class t_fk_user implements DbModel {
	@NotMap
	private static final long serialVersionUID = 1L;
	@Id
	private String kid;
	private int status = 0;
	private Timestamp created = new Timestamp(System.currentTimeMillis());

	private String uname;// 账号
	private String pwd;// 密码

	private String name;// 姓名
	private String sex;
	
	private String type;//类型: 管理员 or 普通用户

	private String role_id;// 角色
	private String status_sys;// 系统状态:正常 or 限制登陆

	// =========== get / set () ====================

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	public String getStatus_sys() {
		return status_sys;
	}

	public void setStatus_sys(String status_sys) {
		this.status_sys = status_sys;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

}
